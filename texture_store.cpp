
#include "texture_store.h"
#include <IL/il.h>
#include <iostream>
using std::cout;
using std::endl;


TextureStore::TextureStore( const std::string & dir )
{
    source_dir = dir;
    num_items = 0;
}

TextureStore::~TextureStore()
{
    for( auto i=store_lib.begin(); i != store_lib.end(); ++i )
        glDeleteTextures( 1, &i->second );
    store_lib.clear();
}


GLuint TextureStore::get( const std::string & fn )
{
    // create GL texture handle
    GLuint texture_handle;     // say this is default/null value (?)
    
    // create IL image handle
    ILuint image;
    ilGenImages(1,&image);
    ilBindImage(image);
    
    // check for presence in library
    try
    {
        texture_handle = store_lib.at(fn);
    }
    catch(...)
    {
        const char * filename = (source_dir+fn).c_str();
        if( ilLoadImage( filename ) )
        {            
            ILuint width, height;
            width = ilGetInteger(IL_IMAGE_WIDTH);
            height = ilGetInteger(IL_IMAGE_HEIGHT);
            ILubyte * img_ptr = ilGetData();
            glGenTextures(1,&texture_handle);
            glBindTexture(GL_TEXTURE_2D,texture_handle);
            glTexImage2D(GL_TEXTURE_2D,
                            0,
                            GL_RGBA,
                            width, height,
                            0, GL_RGBA, GL_UNSIGNED_BYTE,
                            img_ptr);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        }
        else
        {
            cout << "Error: unable to load file "+source_dir+fn << endl;
            exit(1);
        }
        store_lib[fn] = texture_handle;
        num_items += 1;
    }
    
    // delete IL image
    ilDeleteImages(1,&image);
    
    return texture_handle;
}



bool TextureStore::_load_texture( const std::string & fn )
{
    
    
    return true;
}
