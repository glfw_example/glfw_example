#ifndef RENDER_TECH_H
#define RENDER_TECH_H


#include <GL/glew.h>
#ifdef _WIN32
#endif
#include "program_object.h"
#include "model.h"

class Model;    // forward declaration
struct Mesh;

class RenderTech {

    public:
        RenderTech();
        ~RenderTech();
        
        virtual void use() = 0;
        virtual void draw( Model &,
                            glm::mat4 &model,
                            glm::mat4 &view,
                            glm::mat4 &proj   ) = 0;
        virtual void get_vao( Mesh & ) = 0;
    
};



#endif // RENDER_TECH_H
