#ifndef TEXT_RENDER_H
#define TEXT_RENDER_H

#include <ft2build.h>
#include FT_FREETYPE_H

#include "program_object.h"

struct CharacterInfo {
  float ax; // advance.x
  float ay; // advance.y
 
  float bw; // bitmap.width;
  float bh; // bitmap.rows;
 
  float bl; // bitmap_left;
  float bt; // bitmap_top;
 
  float tx; // x offset of glyph in texture coordinates
};

struct Point {
    GLfloat x;
    GLfloat y;
    GLfloat s;
    GLfloat t;
};



class TextRender {

    public:
        TextRender(const std::string&,const std::string&,const std::string&);
        ~TextRender();
        
        void load_face(const std::string&,int);
        void use();
        void draw(  const char *text, float x, float y, float sx, float sy,
                    glm::vec4 color );
        //~ void get_vao( Mesh & );
    
    private:
        FT_Library ft_lib;
        ProgramObject program;
        
        GLint color_loc;
        GLint sampler_loc;
        GLint coord_loc;
        GLuint tex;
        GLuint vbo;
        GLuint vao;
    
        FT_Face face;
        std::string source_dir;
        CharacterInfo char_info[129];
        
        int atlas_width;
        int atlas_height;

};


#endif // TEXT_RENDER_H
