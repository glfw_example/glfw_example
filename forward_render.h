#ifndef FORWARD_RENDER_H
#define FORWARD_RENDER_H

#include "program_object.h"
#include "render_tech.h"

class ForwardRender : public RenderTech {

    public:
        ForwardRender(const std::string&,const std::string&);
        ~ForwardRender();
        
        void use();
        void draw( Model &,
                    glm::mat4 &model,
                    glm::mat4 &view,
                    glm::mat4 &proj   );
        void get_vao( Mesh & );
    
    private:
        ProgramObject program;
        GLint model_loc;
        GLint modelview_loc;
        GLint modelviewproj_loc;
        GLint normalmatrix_loc;
        GLint sampler_loc;

};


#endif // FORWARD_RENDER_H