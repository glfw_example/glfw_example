#include "precomp.hpp"          // external libraries
#include "program_object.h"     // internal libraries
#include "model.h"
#include "object.h"
#include "texture_store.h"
#include "model_store.h"
#include "forward_render.h"
#include "frame_timer.h"
#include "text_render.h"
#include "frame_buffer.h"
#include "screen_render.h"
#include "misc_funcs.h"
#include "camera.h"
#include "locals.hpp"           // 'global' variables
#include "callback_funcs.hpp"   // callbacks



// main program
int main(int argc,char ** argv)
{   
    // store args
    for( int i=0; i<argc; ++i )
        arg_vect.push_back( std::string(argv[i]) );

    parse_args();

    // seed random generator
    srand(clock());

    // init glfw
    glfwInit();
    
    cout << "Num cores: " << std::thread::hardware_concurrency() << endl;
    print_monitor_modes(MON_WIDTH,MON_HEIGHT); // just for fun
 
    // set up a window/context
    glfwWindowHint(GLFW_SAMPLES,0);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

    window = glfwCreateWindow(WINDOW_WIDTH,WINDOW_HEIGHT,"OpenGL",NULL,NULL); // window
        
    glfwMakeContextCurrent(window);

    // attach callbacks and set modes
    glfwSetKeyCallback(window, key_callback);
    glfwSetWindowSizeCallback(window, window_size_callback);
    glfwSetInputMode(window,GLFW_CURSOR,GLFW_CURSOR_HIDDEN);
    
    // set up glew
    glewExperimental = GL_TRUE;
    GLenum status = glewInit();
    if( status != GLEW_OK )
    {
        std::cerr << "[F] GLEW NOT INITIALIZED: ";
        std::cerr << glewGetErrorString(status) << std::endl;
        return -1;
    }
    
    // set up Devil
    ilInit();
    
    glm::mat4 view = glm::lookAt(glm::vec3(20,8.5,20.3),            //Eye Position
                                 glm::vec3(0.0, 0.0, 0.0),      //Focus point
                                 glm::vec3(0.0, 1.0, 0.0));     //Positive Y is up

    projection = glm::perspective( 45.0f, //the FoV typically 90 degre0es is good which is what this is set to
                                   float(WINDOW_WIDTH)/float(WINDOW_HEIGHT), //Aspect Ratio, so Circles stay Circular
                                   0.01f, //Distance to the near plane, normally a small value like this
                                   1000.0f); //Distance to the far plane, 
    glm::mat4 model = glm::translate(glm::mat4(1.0f), glm::vec3(0,-1,-37));
    
    // create render-related objects
    ForwardRender frender(SHADERS+std::string("pass.vert"),SHADERS+std::string("pass.frag"));
    TextRender trender(SHADERS+std::string("text.vert"),SHADERS+std::string("text.frag"),"fonts/");
    trender.load_face("Cousine-Bold.ttf",32);
    
    // make texture/model stores
    TextureStore tex_store("textures/");
    ModelStore model_store("models/",tex_store,frender);
    
    std::vector<Object> objects;
    for( int i=0; i<100; ++i )
    {
        glm::vec3 position = rand_polar(12);
        glm::mat4 t = glm::translate(glm::mat4(1.0f), position);
        Object obj(*model_store.get("object.dae"),t);
        objects.push_back(obj);
    }
    
    // make misc. stuff here
    FrameBuffer fb(1,RENDER_WIDTH,RENDER_HEIGHT);
    ScreenRender srender(SHADERS+std::string("screen.vert"),SHADERS+std::string("screen.frag"));
    
    
    // init GL settings
    glDisable(GL_MULTISAMPLE);
    
    // game timer stuff
    std::string fps_str;
    double ticks, update_ticks=0.0, last_update=0.0, last_frame;
    int loops;
    
    last_frame = ftimer.get_ticks();
    
    // main loop
    while( !glfwWindowShouldClose(window) )
    {
        ftimer.tick();
        
        glfwPollEvents();
        
        // update world
        loops = 0;
        ticks = ftimer.get_ticks();
        while( ticks > last_frame && loops < SKIP_FRAMES_MAX )
        {
            update_ticks = ftimer.get_ticks();
            double dt = update_ticks - last_update; // pass dt to each update function
            for( auto & obj : objects )
                obj.transform = glm::rotate(obj.transform,float(dt)*0.0003f,glm::vec3(0,1,0));
            if( key_map[GLFW_KEY_UP] )
                cam.add(THETA,-0.001f*dt);
            else if( key_map[GLFW_KEY_DOWN] )
                cam.add(THETA,0.001f*dt);
            if( key_map[GLFW_KEY_RIGHT] )
                cam.add(PHI,-0.001f*dt);
            else if( key_map[GLFW_KEY_LEFT] )
                cam.add(PHI,0.001f*dt);
            if( key_map[GLFW_KEY_PAGE_UP] )
                cam.add(RADIUS,-0.01f*dt);
            else if( key_map[GLFW_KEY_PAGE_DOWN] )
                cam.add(RADIUS,0.01f*dt);
            // update view
            view = glm::lookAt(cam.get_pos(),            //Eye Position
                                 glm::vec3(0.0, 0.0, 0.0),      //Focus point
                                 glm::vec3(0.0, 1.0, 0.0));     //Positive Y is up

            last_update = update_ticks;

            if( SHOW_FPS )
                fps_str = std::string("FPS: ") + to_string(ftimer.get_fps());
            
            last_frame += SKIP_FRAMES;
            loops++;
        }
        
        glViewport(0,0,RENDER_WIDTH,RENDER_HEIGHT);
        fb.bind();
        
        // clear screen
        glClearColor(0.0, 0.0, 0.2, 1.0);
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

        // engage renderer
        frender.use();
        
        // render stuff
        for( auto obj : objects )
            frender.draw(*obj.model,obj.transform,view,projection);

        fb.unbind();
        glViewport(0,0,WINDOW_WIDTH,WINDOW_HEIGHT);
        
        srender.render(fb.get_buffer_texture(0));   // transfer buffer to screen

        trender.use();
        // render text
        if( SHOW_FPS )
        {
         
            trender.draw(fps_str.c_str(), -1, 0.95, sx, sy, text_color );
        }
        //~ trender.draw("hello world",0,0,sx,sy,text_color);
        
        // swap buffers and poll events
        if( VSYNC )
            glfwSwapInterval(1);
        glfwSwapBuffers(window);
    }
    
    objects.clear();
    
    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}
