#ifndef PROGRAM_OBJECT_H
#define PROGRAM_OBJECT_H

#include <GL/glew.h>
#ifdef _WIN32
#endif
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>


class ProgramObject {
    
    public:
        
        ProgramObject( std::string, std::string );
        ~ProgramObject();
        
        void bind_attribute_location(int,std::string);
        void bind_frag_data_location(int,std::string);
        void link();
        void use();
        void set_uniform1f(std::string,float);
        void set_uniform3fv(std::string,glm::vec3);
        void set_uniform_matrix4fv(std::string,glm::mat4);
        GLint get_attribute_location(std::string);
        GLint get_uniform_location(std::string);
    
    private:
        
        GLuint vertex_shader;
        GLuint fragment_shader;
        GLuint program;
};


#endif // PROGRAM_OBJECT_H
