#ifndef CALLBACK_FUNCS_HPP
#define CALLBACK_FUNCS_HPP


#include <algorithm>
template<class T>
inline std::string to_string( const T &t ) {
    std::stringstream ss;
    ss << t;
    return ss.str();
}



static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if( action == GLFW_PRESS )
    {
        key_map[key] = true;
    
        if ( key == GLFW_KEY_ESCAPE )
            glfwSetWindowShouldClose(window, GL_TRUE);
        else if( key == GLFW_KEY_F3 )
            SHOW_FPS ^= 1;              // toggle fps display
    }
    else if( action == GLFW_RELEASE )
        key_map[key] = false;
}

static void window_size_callback(GLFWwindow* window, int width, int height)
{
    
    WINDOW_HEIGHT = height;
    WINDOW_WIDTH = width;
    
    //~ float aspect = float(width)/float(height);
    
    projection = glm::perspective( 45.0f, //the FoV typically 90 degrees is good which is what this is set to
                               float(width)/float(height), //Aspect Ratio, so Circles stay Circular
                               0.01f, //Distance to the near plane, normally a small value like this
                               1000.0f); //Distance to the far plane, 
    ftimer.set_fps_res(100000.0f/(WINDOW_WIDTH+WINDOW_HEIGHT));
    //~ proj_ortho = glm::ortho(
}


inline void parse_args()
{
    for( auto i=arg_vect.begin(); i!=arg_vect.end(); i++ )
    {
        if( *i == "--w" )
        {
            i++;
            RENDER_WIDTH = atoi(i->c_str());
        }
        else if( *i == "--h" )
        {
            i++;
            RENDER_HEIGHT = atoi(i->c_str());
        }
        else if( *i == "--vsync" )
        {
            VSYNC = true;
        }
    }
}

#endif // CALLBACK_FUNCS_HPP
