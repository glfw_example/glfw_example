#ifndef SCENE_H
#define SCENE_H

#include "game_state.h"


class Scene : public GameState {

  public:
    Scene();
    ~Scene();
    void handle_input(bool*);
    void update(float&);
    void draw(RenderTech&);



};




#endif // SCENE_H
