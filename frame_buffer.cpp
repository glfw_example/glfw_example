#include "frame_buffer.h"
#include <iostream>
using std::cout;
using std::endl;


FrameBuffer::FrameBuffer(int num_bufs,int width,int height) : 
                                                num_buffers(num_bufs),
                                                buffer_width(width),
                                                buffer_height(height)
{
    // generate and bind this framebuffer
    glGenFramebuffers(1, &fbo_handle);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo_handle);
    
    // generate and attach texture buffers
    buffer_array.reserve(num_buffers);
    for(int i=0; i<num_buffers; ++i)
    {
        GLuint buffer_handle;
                
        glGenTextures(1, &buffer_handle);
        glBindTexture(GL_TEXTURE_2D, buffer_handle);

        glTexImage2D(
            GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL
        );

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        
        glFramebufferTexture2D(
            GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+i, GL_TEXTURE_2D, buffer_handle, 0
        );
        
        buffer_array.push_back(buffer_handle);
    }
    
    // generate and attach render buffer for depth/stencil
    glGenRenderbuffers(1, &rbo_depth_stencil_handle);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo_depth_stencil_handle);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo_depth_stencil_handle);

    
    // bind the default framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


FrameBuffer::~FrameBuffer()
{
    for(int i=0; i<buffer_array.size(); ++i)
        glDeleteTextures( 1, &buffer_array[i] );
    buffer_array.clear();
    glDeleteFramebuffers(1, &fbo_handle);
}


void FrameBuffer::bind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, fbo_handle);
}


void FrameBuffer::bind_for_reading()
{
    glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo_handle);
}

void FrameBuffer::unbind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FrameBuffer::check_status()
{
    if( glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE )
        cout << "Frame Buffer complete!" << endl;
    else
        cout << "Frame Buffer NOT complete!" << endl;
}

GLuint FrameBuffer::get_buffer_texture(int num)
{
    if( num < num_buffers )
        return buffer_array[num];
    else
    {
        cout << "ERROR: attempt to get non-existant buffer texture!" << endl;
        exit(-1);
    }
}

