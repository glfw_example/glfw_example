#version 150    // basic passthru fragment shader

in vec2 UV;

in vec3 m_pos;
in vec3 fN;
in vec3 eyeVec;


out vec4 outColor;

uniform sampler2D textureSampler;

void main()
{
    // hard coded light position
    //~ vec3 fL = vec3(-25,14,-25) - m_pos; // point light
    vec3 fL = vec3(1,1,0.25);               // direction light
    //~ vec3 lightDir = m_pos-fL;
    
    vec3 N = normalize(fN);
    vec3 E = normalize(eyeVec);
    vec3 L = normalize(fL);
    vec3 R = reflect(-L,N);

    float specDot = dot(-R, E);

    
    




    float Kd = max(dot(L,N),0.0);
    float Ka = 0.5;                    // hard coded ambient value
    float Ks = pow(max(specDot,0.0),2);

    vec4 diffuse = texture2D(textureSampler,UV);
    outColor = vec4(Ks*diffuse.xyz+Ka*diffuse.xyz+Kd*diffuse.xyz,1.0);

}
