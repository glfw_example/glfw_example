#version 150
 
in vec2 texcoord;

uniform sampler2D tex;
uniform vec4 color;

out vec4 outColor;
 
void main(void) {

  outColor = vec4(1, 1, 1, texture2D(tex, texcoord).r) * color;

}
