#version 150        // basic passthru vertex shader

in vec3 position;
in vec3 normal;
in vec2 uv_coord;


out vec2 UV;

out vec3 m_pos;
out vec3 fN;
out vec3 eyeVec;


uniform mat4 model;
uniform mat4 model_view;
uniform mat4 model_view_projection;
uniform mat4 normal_matrix;

void main()
{
    m_pos = vec3(model*vec4(position,1.0));
    fN = vec3(normal_matrix*vec4(normal,1.0));
    eyeVec = -vec3(model_view*vec4(position,1.0));

    UV = uv_coord;
    gl_Position = model_view_projection*vec4(position, 1.0);

}
