#ifndef SCREEN_RENDER_H
#define SCREEN_RENDER_H

#include <GL/glew.h>
#ifdef _WIN32
#endif
#include <string>
#include "program_object.h"


class ScreenRender {

    public:
        ScreenRender(const std::string&,const std::string&);
        ~ScreenRender();
        
        void render(GLuint);
    
    private:
        ProgramObject program;
        GLuint sampler_loc;
        GLuint quad_vbo_handle;
        GLuint vao_handle;
};


#endif // SCREEN_RENDER_H
