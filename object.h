#ifndef OBJECT_H
#define OBJECT_H

#include <glm/glm.hpp>
#include "model.h"




class Object {

    public:
        Object( Model &, glm::mat4 t=glm::mat4(1.0f) );
        ~Object();
        
        void translate( glm::vec3 );
        void rotate( float, glm::vec3 );
        void set_position( glm::vec3 );
        void set_orientation( glm::quat );
        void set_orientation( glm::mat4 );
        glm::vec3 get_position();
        glm::quat get_orientation_quat();
        glm::mat4 get_orientation_mat4();
    
        Model * model;
        glm::mat4 transform;
    
};




#endif // OBJECT_H
