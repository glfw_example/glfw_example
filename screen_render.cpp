#include "screen_render.h"

#include <vector>
#include <iostream>
using std::cout;
using std::endl;


ScreenRender::ScreenRender(const std::string &vs,const std::string &fs) :
        program(vs,fs)
{
    // create temporary quad data
    std::vector<GLfloat> quad;
    quad.reserve(24);
    quad = {
        -1.0f,  1.0f,  0.0f, 1.0f,
         1.0f,  1.0f,  1.0f, 1.0f,
         1.0f, -1.0f,  1.0f, 0.0f,

         1.0f, -1.0f,  1.0f, 0.0f,
        -1.0f, -1.0f,  0.0f, 0.0f,
        -1.0f,  1.0f,  0.0f, 1.0f
    };
    
    // generate the screen quad vbo
    glGenBuffers(1, &quad_vbo_handle);
    
    glBindBuffer(GL_ARRAY_BUFFER, quad_vbo_handle); // upload quad data to GPU
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*quad.size(), &quad[0], GL_STATIC_DRAW);
    
    // compile shader program
    program.bind_attribute_location(0,"position");
    program.bind_attribute_location(1,"tex_coord");
    program.bind_frag_data_location(0,"outColor");
    
    program.link();
    
    // store sampler uniform location
    sampler_loc = program.get_uniform_location("texFramebuffer");
    
    // create vao for quad drawing
    glGenVertexArrays(1,&vao_handle);
    glBindVertexArray(vao_handle);
    
    GLint posAttrib = program.get_attribute_location("position");
    GLint uvAttrib = program.get_attribute_location("tex_coord");
    
    glEnableVertexAttribArray(posAttrib);
    glEnableVertexAttribArray(uvAttrib);
    
    glBindBuffer(GL_ARRAY_BUFFER, quad_vbo_handle);
    glVertexAttribPointer(posAttrib,2,GL_FLOAT,GL_FALSE,
                4 * sizeof(GLfloat), 0);
    glVertexAttribPointer(uvAttrib,2,GL_FLOAT,GL_FALSE,
                4 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));
}


ScreenRender::~ScreenRender()
{
    glDeleteBuffers(1, &quad_vbo_handle);
}


void ScreenRender::render(GLuint tex_loc) 
{
    //~ glDisable(GL_STENCIL_TEST);
    glDisable(GL_DEPTH_TEST);

    // Bind default framebuffer and draw contents of our framebuffer
    glBindVertexArray(vao_handle);
    program.use();
    glUniform1f(sampler_loc,0);     // sampler looks in 1st (0th) texture (???)

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex_loc);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 6);
    
}



