#ifndef MODEL_H
#define MODEL_H

#include <GL/glew.h>
#ifdef _WIN32
#endif
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <vector>
#include "texture_store.h"
#include "render_tech.h"


struct Mesh {
    GLuint positions;
    GLuint normals;
    GLuint uv_coords;
    GLuint elements;
    GLuint texture;
    GLuint vao;
    int num_vertices;
    int num_elements;
};


class RenderTech;       // forward decl.

class Model {

    public:
    
        Model( const std::string&, TextureStore &, RenderTech & );
        ~Model();
        
        std::vector<Mesh> meshes;
    
    private:
        
        bool _loadData(const std::string&,TextureStore&,RenderTech&);
        
};



#endif // MODEL_H
