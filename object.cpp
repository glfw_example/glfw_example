#include "object.h"
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>




Object::Object( Model & m, glm::mat4 t )
{
    model = &m;
    transform = t;
}


Object::~Object()
{
    model = NULL;
}





        
void Object::translate( glm::vec3 v )
{
    transform = glm::translate(transform, v);
}

void Object::rotate( float angle, glm::vec3 axis ) 
{
    transform = glm::rotate(transform,angle,axis);
}

void Object::set_position( glm::vec3 v ) 
{
    transform = glm::translate(glm::mat4(1.0f),v) * get_orientation_mat4();
}

void Object::set_orientation( glm::quat q ) 
{
    transform = glm::translate(glm::mat4(1.0f),get_position()) * glm::toMat4(q);
}

void Object::set_orientation( glm::mat4 o )
{
    transform = glm::translate(glm::mat4(1.0f),get_position()) * o;
}

glm::vec3 Object::get_position() 
{
    return glm::vec3(transform[3]);
}

glm::quat Object::get_orientation_quat()
{
    return glm::quat(transform);
}

glm::mat4 Object::get_orientation_mat4()
{
    return glm::toMat4(glm::quat(transform));
}