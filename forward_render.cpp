#include "forward_render.h"

ForwardRender::ForwardRender(const std::string &vs,const std::string &fs) :
        program(vs,fs)
{
    
    program.bind_attribute_location(0,"position");
    program.bind_attribute_location(1,"normal");
    program.bind_attribute_location(2,"uv_coord");
    program.bind_frag_data_location(0,"outColor");
    
    program.link();
    
    sampler_loc = program.get_uniform_location("textureSampler");
    model_loc = program.get_uniform_location("model");
    modelview_loc = program.get_uniform_location("model_view");
    modelviewproj_loc = program.get_uniform_location("model_view_projection");
    normalmatrix_loc = program.get_uniform_location("normal_matrix");
}

ForwardRender::~ForwardRender()
{
    
}

void ForwardRender::use()
{
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    // initialize program
    program.use();
    
    // set render-wide uniforms
    glUniform1f(sampler_loc,0);     // sampler looks in 1st (0th) texture (???)
}


void ForwardRender::draw(   Model & m,
                            glm::mat4 &model,
                            glm::mat4 &view,
                            glm::mat4 &proj   )
{
    // set model-wide uniforms
    glUniformMatrix4fv(modelviewproj_loc, 1, GL_FALSE, glm::value_ptr(proj*view*model));
    glUniformMatrix4fv(modelview_loc, 1, GL_FALSE, glm::value_ptr(view*model));
    glUniformMatrix4fv(model_loc, 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(normalmatrix_loc,1,GL_FALSE,glm::value_ptr(glm::inverse(glm::transpose(model))));
    
    for( auto mesh : m.meshes )
    {
        // bind vertex attribute pointers
        glBindVertexArray(mesh.vao);
        
        // bind texture
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, mesh.texture);

        // bind index array
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,mesh.elements);

        // draw call
        glDrawElements(GL_TRIANGLES, mesh.num_elements, GL_UNSIGNED_INT, (void*)0);
    }
    
}


void ForwardRender::get_vao( Mesh & m )
{
    GLuint vao_handle;
    
    glGenVertexArrays(1,&vao_handle);
    glBindVertexArray(vao_handle);
    
    GLint posAttrib = program.get_attribute_location("position");
    GLint normAttrib = program.get_attribute_location("normal");
    GLint uvAttrib = program.get_attribute_location("uv_coord");
    
    glEnableVertexAttribArray(posAttrib);
    glEnableVertexAttribArray(normAttrib);
    glEnableVertexAttribArray(uvAttrib);
    
    glBindBuffer(GL_ARRAY_BUFFER, m.positions);
    glVertexAttribPointer(posAttrib,3,GL_FLOAT,GL_FALSE,0,0);
    glBindBuffer(GL_ARRAY_BUFFER, m.normals);
    glVertexAttribPointer(normAttrib,3,GL_FLOAT,GL_FALSE,0,0);
    glBindBuffer(GL_ARRAY_BUFFER, m.uv_coords);
    glVertexAttribPointer(uvAttrib,2,GL_FLOAT,GL_FALSE,0,0);
    
    m.vao = vao_handle;
}
