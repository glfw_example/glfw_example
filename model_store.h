#ifndef MODEL_STORE_H
#define MODEL_STORE_H

#include "model.h"

#include <map>
#include <string>


typedef std::map<std::string,Model *> ModelMap;

class ModelStore {

    public:
        ModelStore( const std::string &,TextureStore &, RenderTech & );
        ~ModelStore();
        Model * get( const std::string & );
        
        int num_items;
        
    private:
        ModelMap store_lib;
        std::string source_dir;
        TextureStore * tex_store;
        RenderTech * render_tech;

};


#endif // MODEL_STORE_H
