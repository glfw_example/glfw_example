#include "misc_funcs.h"
#include <cstdlib>


#define PI 3.14159265359f

//--text file reader for shader loading
std::string read_file( std::string filename )
{
    // init variables
    std::ifstream infile(filename.c_str());
    std::string tmpStr;
    std::string * name = new std::string("");
    
    getline(infile,tmpStr);
    while( infile.good() )
    {
        *name += tmpStr;
        *name += '\n';
        
        getline(infile,tmpStr);
    }
    
    *name += '\n';
    
    infile.close();
    
    return (*name);
}

void read_shader( std::string filename, std::string & shader )
{
    // init variables
    std::ifstream infile(filename);
    std::string tmpStr;
    std::string name("");

    getline(infile,tmpStr);
    while( infile.good() ) {
        
        name += tmpStr;
        name += "\n";
        
        getline(infile,tmpStr);
    }
    
    name += "\n";
    
    shader = (char*)name.c_str();
    
    infile.close();
}


int get_file_length( std::string filename )
{
    // init variables
    std::ifstream infile(filename.c_str());
    std::string tmpStr;
    std::string name("");
    
    while( infile.good() ) {
        getline(infile,tmpStr);
        
        name += tmpStr;
        name += "\n";
    }
    
    infile.close();
    
    return name.length();
}


void check_compile_status( GLuint &shader )
{
    GLint status;
    glGetShaderiv( shader, GL_COMPILE_STATUS, &status );
    if( !status )
    {
        std::cerr << "[F] FAILED TO COMPILE SHADER!" << endl;
        char buffer[512];
        glGetShaderInfoLog(shader, 512, NULL, buffer);
        cout << buffer << endl;
        exit(-1);
    }
}


void check_link_status( GLuint &shader )
{
    GLint status;
    glGetProgramiv( shader, GL_LINK_STATUS, &status );
    if( !status )
    {
        std::cerr << "[F] FAILED TO LINK PROGRAM!" << endl;
        char buffer[512];
        glGetProgramInfoLog(shader, 512, NULL, buffer);
        cout << buffer << endl;
        exit(-1);
    }
}


float randf()
{
    return float(rand())/RAND_MAX;
}



glm::vec3 rand_vec3(float min,float max)
{
    float range = fabs(max-min);
    return glm::vec3(min+randf()*range,min+randf()*range,min+randf()*range);
}


glm::vec3 rand_polar(float r)
{
    float r_new = randf() * r;
    float theta = randf() * PI;
    float phi = randf() * 2.0f * PI;
    return polar_to_vec3(r_new,theta,phi);
}



void print_monitor_modes(int &cur_width,int &cur_height)
{
    GLFWmonitor * monitor = glfwGetPrimaryMonitor();
    int vidmode_count;
    const GLFWvidmode * vidmodes = glfwGetVideoModes(monitor,&vidmode_count);
    const GLFWvidmode * curmode = glfwGetVideoMode(monitor);
    cout << "Available modes:" << endl;
    for(int i=0; i<vidmode_count; ++i)\
    {
        cout << vidmodes[i].width << ' '
            << vidmodes[i].height << ' '
            << vidmodes[i].refreshRate;
        if( curmode->width == vidmodes[i].width &&
            curmode->height == vidmodes[i].height &&
            curmode->refreshRate == vidmodes[i].refreshRate )
            cout << " *" << endl;
        else
            cout << endl;
    }
    cur_width = curmode->width;
    cur_height = curmode->height;
}



glm::vec3 vec3_to_polar(glm::vec3 p)
{
    float r, theta, phi;
    
    r = sqrtf( p.x*p.x + p.y*p.y + p.z*p.z );
    theta = glm::acos(p.y/r);
    phi = glm::atan(p.z/r);
    
    return glm::vec3(r,theta,phi);
}


glm::vec3 polar_to_vec3(float r,float theta,float phi)
{
    float x, y, z;
    
    x = r * glm::sin(theta) * glm::cos(phi);
    y = r * glm::sin(theta) * glm::sin(phi);
    z = r * glm::cos(theta);
    
    return glm::vec3(x,z,y);    
}




