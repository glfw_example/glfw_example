#include "model_store.h"



ModelStore::ModelStore( const std::string & src_dir,
                        TextureStore & tstore, RenderTech & rtech )
{
    source_dir = src_dir;
    tex_store = &tstore;
    render_tech = &rtech;
    num_items = 0;
}



ModelStore::~ModelStore()
{
    store_lib.clear();
}



Model * ModelStore::get( const std::string & name )
{
    Model * tmpModel;
    
    try
    {
         tmpModel = store_lib.at(name);
    }
    catch(...)
    {
        std::string filename = source_dir + name;
        
        tmpModel = new Model(filename,*tex_store,*render_tech);
        
        store_lib[name] = tmpModel;
        num_items += 1;
    }
    
    return tmpModel;
}