#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>

#define PI 3.14159265359f

enum  POLAR_COORDS { RADIUS, THETA, PHI };

class Camera {

    public:
        Camera(float r_in=15.0f, float theta_in=PI/2.0f, float phi_in=PI);
        ~Camera();
        
        float r;
        float theta;
        float phi;
        
        void add(int,float);
        glm::vec3 get_pos();
        void set_pos(glm::vec3);
};



#endif // CAMERA_H
