#ifndef MISC_FUNCS_H
#define MISC_FUNCS_H

#include <GL/glew.h>
#ifdef _WIN32
#endif
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <fstream>
#include <string>
#include <iostream>

using std::endl;
using std::cout;


// prototypes
std::string read_file( std::string filename );
void read_shader( std::string filename, std::string & shader );
int get_file_length( std::string filename );
void check_compile_status( GLuint &shader );
void check_link_status( GLuint &shader );
float randf();
glm::vec3 rand_vec3(float,float);
glm::vec3 rand_polar(float);
void print_monitor_modes(int&,int&);
glm::vec3 vec3_to_polar(glm::vec3);
glm::vec3 polar_to_vec3(float,float,float);


#endif // MISC_FUNCS_HPP
