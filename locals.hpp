#ifndef LOCALS_HPP
#define LOCALS_HPP


#define PI 3.14159265359f

// directories
#define SHADERS std::string("shaders/")

// initial window size
int RENDER_WIDTH = 1024;
int RENDER_HEIGHT = 576;
int WINDOW_WIDTH = 1280;
int WINDOW_HEIGHT = 720;
int MON_WIDTH;
int MON_HEIGHT;

float proj_aspect = float(RENDER_HEIGHT)/2.0f;

int num_keys = 400;
bool * key_map = new bool [num_keys];

glm::mat4 projection;

float sx = 1.0f/RENDER_WIDTH;
float sy = 1.0f/RENDER_HEIGHT;

const int FPS                   = 60;
const double SKIP_FRAMES        = 1000.0/FPS;
const int SKIP_FRAMES_MAX       = 5;

bool VSYNC = false;
bool SHOW_FPS = true;
GLFWwindow * window;

std::vector<std::string> arg_vect;

FrameTimer ftimer(1000*1000.0f/(WINDOW_WIDTH+WINDOW_HEIGHT));
glm::vec4 text_color(0,0.85,0.99,1.0);


Camera cam;


#endif // LOCALS_HPP
