#include "camera.h"
#include "misc_funcs.h"



Camera::Camera(float r_in, float theta_in, float phi_in) :
                                            r(r_in),
                                            theta(theta_in),
                                            phi(phi_in)
{
    
}



Camera::~Camera()
{
    
}


void Camera::add(int choice,float factor) 
{
    switch(choice)
    {
        case RADIUS:
            r += factor;
            if( r < 0.01f ) r = 0.01f;
            break;
        case THETA:
            theta += factor * (1.0f+1.0f/r);
            if( theta < 0.0001f ) theta = 0.0001f;
            else if( theta > PI-0.0001f ) theta = PI-0.0001f;
            break;
        case PHI:
            phi += factor * (1.0f+1.0f/r);
            break;
    }
}


glm::vec3 Camera::get_pos()
{
    return polar_to_vec3(r,theta,phi);
}

void Camera::set_pos(glm::vec3 p)
{
    glm::vec3 new_pos = vec3_to_polar(p);
    r = new_pos.x;
    theta = new_pos.y;
    phi = new_pos.z;
}
