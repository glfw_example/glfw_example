#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "render_tech.h"

class GameState {
  
  public:
    virtual void handle_input(bool*) = 0;
    virtual void update(float&) = 0;
    virtual void draw(RenderTech&) = 0;

    GameState * next;
    bool exit;
};



#endif
