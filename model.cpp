#include <assimp/cimport.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <iostream>
#include "model.h"

using std::endl;
using std::cout;


Model::Model( const std::string& fn, TextureStore & tex_store, RenderTech & rtech )
{
    if( !_loadData(fn,tex_store,rtech) )
    {
        cout << "Cannot load file " << fn << endl;
        exit(-1);
    }
}



bool Model::_loadData( const std::string & fn, TextureStore & tex_store, RenderTech & rtech )
{
    // import file using Assimp
    const aiScene * scene = aiImportFile( fn.c_str(),
                        aiProcess_Triangulate|aiProcess_JoinIdenticalVertices );
    
    // return false if scene didn't load correctly
    if( !scene )
    {
        cout << "Error: couldn't load model file" << endl;
        return false;
    }

    // return false if there are no meshes
    if( !scene->HasMeshes() )
    {
        cout << "Error: no meshes in model file" << endl;
        return false;
    }
    
    // false if there are no normals
    if( !scene->mMeshes[0]->HasNormals() )
    {   
        cout << "Error: meshes lack normals" << endl;
        return false;
    }
    
    // allocate model data
    meshes.reserve(scene->mNumMeshes);
    
    // iterate over materials/load textures
    std::vector<GLuint> texVect;    // map material number to texture handle 
    texVect.reserve(scene->mNumMaterials);
    for (int i = 0; i<scene->mNumMaterials; ++i)
    {
        aiMaterial * aimat = scene->mMaterials[i];
        
        GLuint tex_handle;
        
        std::string * sName;

        // load textures
        if(aimat->GetTextureCount(aiTextureType_DIFFUSE) > 0)
        {
            aiString * name = new aiString;
            // get name of 1st diffuse texture in material
            aimat->GetTexture(aiTextureType_DIFFUSE,0,name);
            sName = new std::string(name->C_Str());
        }
        else
        {
            // name 'null' if no texture present in material
            sName = NULL;
        }
        
        if( sName != NULL )
        {
            tex_handle = tex_store.get(*sName);
        }
        else
            tex_handle = 0;
        
        // store texture name in local array
        texVect[i] = tex_handle;
    }

    // iterate over meshes
    meshes.reserve(scene->mNumMeshes);
    for ( int i=0; i<scene->mNumMeshes; ++i )
    {
        // allocate new mesh data
        aiMesh * am = scene->mMeshes[i];
        Mesh m;
        
        // reserve data to be uploaded
        std::vector<glm::vec3> positions;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> uv_coords;
        std::vector<unsigned int> elements;
        
        // set texture data from local array
        m.texture = texVect[am->mMaterialIndex];
        m.num_vertices = am->mNumVertices;
        m.num_elements = am->mNumFaces * 3;
        
        // reserve data
        positions.reserve( am->mNumVertices );
        normals.reserve( am->mNumVertices );
        uv_coords.reserve( am->mNumVertices );
        elements.reserve( am->mNumFaces * 3 );

        // iterate over vertices
        for( int j=0; j<am->mNumVertices; ++j )
        {
            // get ai vector types
            aiVector3D p = am->mVertices[j];
            aiVector3D n = am->mNormals[j];
            aiVector3D uv = am->mTextureCoords[0][j];
            
            // convert to glm vector types, add to lists
            positions.push_back(glm::vec3(p.x,p.y,p.z));
            normals.push_back(glm::vec3(n.x,n.y,n.z));
            uv_coords.push_back(glm::vec2(uv.x,uv.y));
        }

        // iterate over faces
        for ( int j=0; j<am->mNumFaces; ++j )
        {
            // get pointer to ai face
            aiFace af = am->mFaces[j];
            
            // check for triangle faces
            assert( af.mNumIndices == 3 );
            
            // iterate over face indices
            for ( int k=0; k<3; ++k )
                elements.push_back( af.mIndices[k] );
        }
        
        // upload data to GPU
        GLuint vbo_pos;
        glGenBuffers(1, &vbo_pos);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_pos);
        glBufferData(   GL_ARRAY_BUFFER,
                        positions.size()*sizeof(glm::vec3),
                        &positions[0],
                        GL_STATIC_DRAW);

        GLuint vbo_normals;
        glGenBuffers(1, &vbo_normals);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_normals);
        glBufferData(   GL_ARRAY_BUFFER, 
                        normals.size()*sizeof(glm::vec3),
                        &normals[0],
                        GL_STATIC_DRAW);

        GLuint vbo_uvs;
        glGenBuffers(1, &vbo_uvs);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_uvs);
        glBufferData(   GL_ARRAY_BUFFER,
                        uv_coords.size() * sizeof(glm::vec2), 
                        &uv_coords[0],
                        GL_STATIC_DRAW);

        GLuint ebo;
        glGenBuffers(1,&ebo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,ebo);
        glBufferData(   GL_ELEMENT_ARRAY_BUFFER,
                        elements.size() * sizeof(unsigned int),
                        &elements[0],
                        GL_STATIC_DRAW);
        
        // set vbo handles in mesh struct
        m.positions = vbo_pos;
        m.normals = vbo_normals;
        m.uv_coords = vbo_uvs;
        m.elements = ebo;
        rtech.get_vao( m );
        
        // add mesh to model data
        meshes.push_back(m);
    }
    
    // release Assimp scene resource
    aiReleaseImport( scene );  
    
    return true;
}





Model::~Model()
{
    // destroy mesh data
    for( auto m : meshes )
    {
        glDeleteBuffers(1, &m.positions);
        glDeleteBuffers(1, &m.normals);
        glDeleteBuffers(1, &m.uv_coords);
        glDeleteBuffers(1, &m.elements);
        glDeleteVertexArrays(1, &m.vao);
    }
}
    


