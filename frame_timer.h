#ifndef FRAME_TIMER_H
#define FRAME_TIMER_H

#include <GLFW/glfw3.h>


class FrameTimer {

    public:
        FrameTimer(int res_in=60);
        
        void tick();
        float get_fps();
        void set_fps_res(int);
        
        double get_ticks();
    
    private:
        double last_tick,current_tick,ticks;
        int tick_count;
        float current_fps;
        float avg_fps;
        int fps_res;
};




#endif // FRAME_TIMER_H
