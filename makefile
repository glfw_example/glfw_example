LIBS = -lassimp -pthread -lglfw -lGLEW -lGL -lIL -lfreetype 
INCLUDES = -I/usr/include/freetype2 
CFLAGS = -Wno-deprecated -std=c++11
CC = clang++ 
#CC = g++
EXE = main.out

HLIST = $(shell ls *.h)
OBJECTS = $(HLIST:.h=.o)

main: $(OBJECTS) driver.cpp
	$(CC) -o $(EXE) $(OBJECTS) driver.cpp $(LIBS) $(INCLUDES) $(CFLAGS)
%.o: %.cpp %.h
	$(CC) $(CFLAGS) $(INCLUDES) -c $<
	

clean:
	rm main.out; rm *.o


