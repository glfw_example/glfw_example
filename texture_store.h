#ifndef TEXTURE_STORE_H
#define TEXTURE_STORE_H

#include <GL/glew.h>
#ifdef _WIN32
#endif
#include <string>
#include <map>


typedef std::map<std::string,GLuint> TextureMap;


class TextureStore {

    public:
        TextureStore( const std::string & );
        ~TextureStore();
        GLuint get( const std::string & );
        
        int num_items;
        
    private:
        TextureMap store_lib;
        std::string source_dir;
        bool _load_texture( const std::string & );
};






#endif // TEXTURE_STORE_H
