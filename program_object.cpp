#include "program_object.h"
#include "misc_funcs.h"



ProgramObject::ProgramObject( std::string vert_file, std::string frag_file )
{
    // read in shader sources
    std::string tmp_vs;
    tmp_vs = read_file( vert_file );
    int vs_length = tmp_vs.length();
    const char * vertex_src = tmp_vs.c_str();

    std::string tmp_fs;
    tmp_fs = read_file( frag_file );
    int fs_length = tmp_fs.length();
    const char * fragment_src = tmp_fs.c_str();

    // create shader objects
    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    
    // compile shaders
    glShaderSource(vertex_shader, 1, &vertex_src, &vs_length);
    glCompileShader(vertex_shader);
    check_compile_status(vertex_shader);
    glShaderSource(fragment_shader, 1, &fragment_src, &fs_length);
    glCompileShader(fragment_shader);
    check_compile_status(fragment_shader);
    
    // create program object
    program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
}



ProgramObject::~ProgramObject()
{
    glDeleteProgram(program);
    glDeleteShader(fragment_shader);
    glDeleteShader(vertex_shader);
}


void ProgramObject::use()
{
    glUseProgram(program);
}


void ProgramObject::bind_attribute_location(int location,std::string name)
{
    glBindAttribLocation(program,location,name.c_str());
}


void ProgramObject::bind_frag_data_location(int location,std::string name)
{
    glBindFragDataLocation( program, location, name.c_str() );
}


GLint ProgramObject::get_attribute_location(std::string name)
{
    return glGetAttribLocation(program, name.c_str());
}

GLint ProgramObject::get_uniform_location(std::string name)
{
    return glGetUniformLocation(program, name.c_str());
}


void ProgramObject::link()
{
    // link program
    glLinkProgram(program);
    check_link_status(program);
}

void ProgramObject::set_uniform1f(std::string name,float num)
{
    GLint uniLoc = glGetUniformLocation(program, name.c_str());
    glUniform1f(uniLoc, num);
}

void ProgramObject::set_uniform3fv(std::string name,glm::vec3 val) 
{
    GLint uniLoc = glGetUniformLocation(program, name.c_str());
    glUniform3fv(uniLoc, 1, glm::value_ptr(val));
}

