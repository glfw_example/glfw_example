#include "frame_timer.h"



FrameTimer::FrameTimer(int res_in)
{
    fps_res = res_in;
    current_fps = 0;
    tick_count = 0;
    last_tick = glfwGetTime();
    
}

void FrameTimer::tick()
{
    current_tick = glfwGetTime();
    ticks = current_tick - last_tick;
    
    current_fps += 1.0f/ticks;
    tick_count++;
    
    last_tick = current_tick;
}

void FrameTimer::set_fps_res(int res_in)
{
    fps_res = res_in;
}

float FrameTimer::get_fps()
{
    if( tick_count > fps_res )
    {
        avg_fps = current_fps/tick_count;
        tick_count = 0;
        current_fps = 0.0f;
    }
    return avg_fps;
}


double FrameTimer::get_ticks()
{
    return glfwGetTime()*1000.0;    // ticks in milliseconds
}
