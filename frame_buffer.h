#ifndef FRAME_BUFFER_H
#define FRAME_BUFFER_H

#include <GL/glew.h>
#ifdef _WIN32
#endif
#include <vector>

class FrameBuffer {

    public:
        FrameBuffer(int num_bufs=1,int width=640,int height=480);
        ~FrameBuffer();
        
        void bind();
        void bind_for_reading();
        void unbind();
        void check_status();
        GLuint get_buffer_texture(int);
    
    private:
        GLuint fbo_handle;
        int num_buffers;
        std::vector<GLuint> buffer_array;
        GLuint rbo_depth_stencil_handle;

        unsigned short buffer_width;
        unsigned short buffer_height;

};



#endif // FRAME_BUFFER_H
