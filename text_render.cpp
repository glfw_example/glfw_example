#include "text_render.h"
#include <iostream>
using std::cout;
using std::endl;



TextRender::TextRender( const std::string& vs,
                        const std::string& fs,
                        const std::string& src_dir ) : 
                program(vs,fs)
{
    // store source dir for fonts
    source_dir = src_dir;
    
    // initiate freetype library
    if( FT_Init_FreeType(&ft_lib) )
    {
        fprintf(stderr, "Could not init freetype library\n");
        exit(-1);
    }

    atlas_width = -1;
    atlas_height = -1;

    // initiate shader program
    program.bind_attribute_location(0,"coord");
    program.bind_frag_data_location(0,"outColor");
    
    program.link();
    
    sampler_loc = program.get_uniform_location("tex");
    color_loc = program.get_uniform_location("color");
    coord_loc = program.get_attribute_location("coord");
    
    //~ glGenTextures(1, &tex);
    glGenBuffers(1, &vbo);
    
    glGenVertexArrays(1,&vao);
    glBindVertexArray(vao);
        
    glEnableVertexAttribArray(coord_loc);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(coord_loc, 4, GL_FLOAT, GL_FALSE, 0, 0);

    
}


TextRender::~TextRender()
{
    
}


void TextRender::load_face( const std::string& fn, int size )
{
    // delete current texture atlas if present
    if( atlas_width >= 0 )
        glDeleteTextures( 1, &tex );
    
    // load new face into library
    if( FT_New_Face(ft_lib,(source_dir+fn).c_str(),0,&face) )
    {
        fprintf(stderr, "Could not open font\n");
        exit(-1);
    }
    
    FT_Set_Pixel_Sizes(face, 0, size);
    
    // set glyph pointer
    FT_GlyphSlot g;
    int w = 0;
    int h = 0;
     
    for(int i=32; i<128; ++i) 
    {
        if( FT_Load_Char(face, i, FT_LOAD_RENDER) )
        {
            fprintf(stderr, "Loading character %c failed!\n", i);
            exit(-1);
        }
        
        g = face->glyph;
     
        w += g->bitmap.width;
        h = std::max(h, g->bitmap.rows);
     
    }
    atlas_width = w;
    atlas_height = h;

    // create texture atlas
    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RED, GL_UNSIGNED_BYTE, 0);
    
    // paste character glyphs into texture atlas
    int x = 0; 
    for(int i=32; i < 128; ++i)
    {
        if(FT_Load_Char(face, i, FT_LOAD_RENDER))
            continue;
        
        g = face->glyph;
     
        glTexSubImage2D(GL_TEXTURE_2D, 0, x, 0, g->bitmap.width, g->bitmap.rows, GL_RED, GL_UNSIGNED_BYTE, g->bitmap.buffer);
      
        char_info[i].ax = g->advance.x >> 6;
        char_info[i].ay = g->advance.y >> 6;

        char_info[i].bw = g->bitmap.width;
        char_info[i].bh = g->bitmap.rows;

        char_info[i].bl = g->bitmap_left;
        char_info[i].bt = g->bitmap_top;

        char_info[i].tx = (float)x / w;
        
        
        x += g->bitmap.width;
    }
    
}



void TextRender::use()
{    
    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    program.use();
    glBindVertexArray(vao);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glUniform1i(sampler_loc, 0);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
}

void TextRender::draw(  const char *text, float x, float y, float sx, float sy,
                        glm::vec4 color )
{
    
    Point coords[6 * strlen(text)];
    
    glUniform4fv(color_loc, 1, glm::value_ptr(color));

    int n = 0;
    int len = strlen(text);

    for(int i=0; i<len; ++i)
    { 
        char p = text[i];
        float x2 =  x + char_info[p].bl * sx;
        float y2 = -y - char_info[p].bt * sy;
        float w = char_info[p].bw * sx;
        float h = char_info[p].bh * sy;

        /* Advance the cursor to the start of the next character */
        x += char_info[p].ax * sx;
        y += char_info[p].ay * sy;

        /* Skip glyphs that have no pixels */
        if(!w || !h)
            continue;

        coords[n++] = (Point){x2,     -y2    , char_info[p].tx,                                            0};
        coords[n++] = (Point){x2 + w, -y2    , char_info[p].tx + char_info[p].bw / atlas_width,   0};
        coords[n++] = (Point){x2,     -y2 - h, char_info[p].tx,                                          char_info[p].bh / atlas_height}; //remember: each glyph occupies a different amount of vertical space
        coords[n++] = (Point){x2 + w, -y2    , char_info[p].tx + char_info[p].bw / atlas_width,   0};
        coords[n++] = (Point){x2,     -y2 - h, char_info[p].tx,                                          char_info[p].bh / atlas_height};
        coords[n++] = (Point){x2 + w, -y2 - h, char_info[p].tx + char_info[p].bw / atlas_width,         char_info[p].bh / atlas_height};
    }


    glBufferData(GL_ARRAY_BUFFER, sizeof coords, coords, GL_DYNAMIC_DRAW);
    glDrawArrays(GL_TRIANGLES, 0, n);
}


////////////////////////////////////////////////////////////
    //~ const char *p;
//~ 
    //~ FT_GlyphSlot g;
//~ 
    //~ g = face->glyph;
    //~ 
    //~ for(p = text; *p; p++)
    //~ {
        //~ if(FT_Load_Char(face, *p, FT_LOAD_RENDER))
            //~ exit(-1);
//~ 
        //~ glTexImage2D(
            //~ GL_TEXTURE_2D,
            //~ 0,
            //~ GL_RGB,
            //~ g->bitmap.width,
            //~ g->bitmap.rows,
            //~ 0,
            //~ GL_RED,
            //~ GL_UNSIGNED_BYTE,
            //~ g->bitmap.buffer
        //~ );
//~ 
        //~ float x2 = x + g->bitmap_left * sx;
        //~ float y2 = -y - g->bitmap_top * sy;
        //~ float w = g->bitmap.width * sx;
        //~ float h = g->bitmap.rows * sy;
//~ 
        //~ GLfloat box[4][4] = {
            //~ {x2,     -y2    , 0, 0},
            //~ {x2 + w, -y2    , 1, 0},
            //~ {x2,     -y2 - h, 0, 1},
            //~ {x2 + w, -y2 - h, 1, 1},
        //~ };
//~ 
        //~ glBufferData(GL_ARRAY_BUFFER, sizeof box, box, GL_DYNAMIC_DRAW);
        //~ glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
//~ 
        //~ x += (g->advance.x >> 6) * sx;
        //~ y += (g->advance.y >> 6) * sy;
    //~ }
//~ }
