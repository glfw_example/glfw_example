#ifndef PRECOMP_HPP
#define PRECOMP_HPP

// glew/glfw
#include <GL/glew.h>
#ifdef _WIN32
#endif
#include <GLFW/glfw3.h>

// Devil image loading
#include <IL/il.h>

// openGL math
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// std libs
#include <thread>
#include <iostream>
#include <vector>
#include <sstream>

using std::cout;
using std::endl;

#endif // PRECOMP_HPP
